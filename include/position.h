//#include "geometry_msgs/Pose2D.h"

class Position{
	public:
		Position();
		Position(const double x,const double y,const double theta);
//		Position(geometry_msgs::Pose2D &pose);
		double getX() const;
		double getY() const;
		double getTheta() const;
		void setX(const double x);
		void setY(const double y);
		void setTheta(const double theta);
	private:
		double x;
	 	double y;
	 	double theta;
};