#include <ros/ros.h>
#include <actionlib/server/simple_action_server.h>
#include <stdlib.h>
#include <cmath>
#include <iostream>
#include <fstream>
#include <lta_semantic_map/AddDoorAction.h>
#include <lta_semantic_map/UpdateDoorAction.h>
#include <lta_semantic_map/RemoveDoorAction.h>
#include <lta_semantic_map/SaveDoorDBAction.h>
#include <lta_semantic_map/DoorEntry.h>
#include <lta_semantic_map/DoorMap.h>
//#include OPRS STUFF

#ifndef DOOR_H
#define DOOR_H
#include "door.h"
#endif
#ifndef STRING
#define STRING
#include <string>
#endif

std::string door_db_filename = "~/catkin_ws/src/lta_semantic_map/test_doors.yaml";
ros::Publisher db_publisher;
std::map<int,Door*> door_db;
const std::string POSITION_LABEL = "position:";
const std::string X_POSITION_LABEL = "x:";
const std::string Y_POSITION_LABEL = "y:";
const std::string THETA_POSITION_LABEL = "theta:";
const std::string WIDTH_LABEL = "width:";
const std::string STATUS_LABEL = "status:";
const std::string OPEN_PROB_LABEL = "open_prob:";
const std::string CLOSED_PROB_LABEL = "closed_prob:";

typedef actionlib::SimpleActionServer<lta_semantic_map::AddDoorAction> AddServer;
typedef actionlib::SimpleActionServer<lta_semantic_map::RemoveDoorAction> RemoveServer;
typedef actionlib::SimpleActionServer<lta_semantic_map::UpdateDoorAction> UpdateServer;
typedef actionlib::SimpleActionServer<lta_semantic_map::SaveDoorDBAction> SaveDBServer;

bool load_door_db(std::string yaml);
bool store_door_db(std::string yaml);

int add_door(const Position position, const double width, const Door::Door_State state, const double prob_open, const double prob_closed);
bool remove_door(int id);
bool update_door_pos(int id, const Position position);
bool update_door_width(int id, const double width);
bool update_door_state(int id, const Door::Door_State state);
bool update_door_prob_open(int id, const double prob_open);
bool update_door_prob_closed(int id, const double prob_closed);

void execute_add(const lta_semantic_map::AddDoorGoalConstPtr& goal, AddServer* as);
void execute_update(const lta_semantic_map::UpdateDoorGoalConstPtr& goal, UpdateServer* as);
void execute_remove(const lta_semantic_map::RemoveDoorGoalConstPtr& goal, RemoveServer* as);
void execute_storedb(const lta_semantic_map::SaveDoorDBGoalConstPtr& goal, SaveDBServer* as);

lta_semantic_map::DoorMap make_db_message();