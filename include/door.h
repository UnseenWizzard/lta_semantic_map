#ifndef POS_H
#define POS_H
#include "position.h"
#endif
#ifndef STRING
#define STRING
#include <string>
#endif
#ifndef SSTREAM
#define SSTREAM
#include <sstream>
#endif


class Door{
	public:
		enum Door_State { OPEN, CLOSED, UNKNOWN};
		Door();
		//~Door();
		Door(const int id, const Position position, const double width, const Door_State state, const double prob_open, const double prob_closed);
		int getID() const;
		Position getPosition() const;
		double getWidth() const;
		bool isOpen() const;
		bool isClosed() const;
		Door_State getDoorState() const;
		double getProbabiltyOpen() const;
		double getProbabiltyClosed() const;
		std::string getYamlString() const;
		void setID(const int id);
		void setPosition(const Position pos);
		void setWidth(const double width);
		void setOpen();
		void setClosed();
		void setUnknown();
		void setDoorState(const Door_State state);
		void setProbabiltyOpen(const double probabilty);
		void setProbabiltyClosed(const double probabilty);
	private:
	 int id;
	 Position position;
	 double width;
	 Door_State state;
	 double prob_open;
	 double prob_closed;
};