#ifndef GRID_LAYER_H_
#define GRID_LAYER_H_
#include <ros/ros.h>
#include <ros/callback_queue.h>
#include <stdlib.h>
#include <cmath>
#include <costmap_2d/layer.h>
#include <costmap_2d/layered_costmap.h>
#include <costmap_2d/GenericPluginConfig.h>
#include <dynamic_reconfigure/server.h>
#include <lta_semantic_map/DoorEntry.h>
#include <lta_semantic_map/DoorMap.h>

#ifndef DOOR_H
#define DOOR_H
#include "door.h"
#endif
#ifndef STRING
#define STRING
#include <string>
#endif

//todo rename
namespace lta_semantic_layer_namespace
{
	class DoorLayer : public costmap_2d::Layer, public costmap_2d::Costmap2D {
		public:
			DoorLayer();
			virtual void onInitialize();
			virtual void updateBounds(double robot_x, double robot_y, double robot_yaw, double* min_x, double* min_y, double* max_x, double* max_y);
  			virtual void updateCosts(costmap_2d::Costmap2D& master_grid, int min_i, int min_j, int max_i, int max_j);
  			bool isDiscretized(){ return true; }
  			virtual void reset();
			virtual void matchSize();
		private:
			ros::NodeHandle nh;
			ros::Subscriber sub;
			std::map<int,Door*> door_db;
			void door_db_message_callback(const lta_semantic_map::DoorMapConstPtr& msg);
			Position get_end_point(Position p, double width);
  			void reconfigureCB(costmap_2d::GenericPluginConfig &config, uint32_t level);
  			dynamic_reconfigure::Server<costmap_2d::GenericPluginConfig> *dsrv_;
	};
}
#endif