//Mock Door Sensor Plugin
/*idea: 
	not a plugin. but a rosnode
	subscribes to odometry position and laser data from stage
	if laser reading/no laser reading, but in front of a known door (from list as elsewhere)
		with 90% probability, set the entry in the semantic map door db to closed/open
*/
#include <ros/ros.h>
#include <stdlib.h>
#include <cmath>
#include <fstream>
#include <iostream>
#ifndef STRING
#define STRING
#include <string>
#endif
#include <random>
#ifndef DOOR_H
#define DOOR_H
#include "door.h"
#endif
 
#include <actionlib/client/simple_action_client.h>
#include <lta_semantic_map/UpdateDoorAction.h>

std::map<int,Door*> door_db;

void door_db_message_callback(const lta_semantic_map::DoorMap::ConstPtr& msg){
	ROS_INFO("GOT DB MSG");
    door_db.clear();
	for (auto &entry : msg->door_db){
        Door::Door_State state = Door::UNKNOWN;
        if (entry.door_state.find("OPEN")!=std::string::npos){
          state = Door::OPEN;
        } else if (entry.door_state.find("CLOSED")!=std::string::npos){
          state = Door::CLOSED;
        }
		door_db[entry.door_id] = new Door(entry.door_id, Position(entry.pos_x,entry.pos_y,entry.pos_theta), entry.width, state, entry.prob_open, entry.prob_closed);
        //ROS_INFO("Storing door %d",entry.door_id);
		}
    ROS_INFO("DB size %lu",door_db.size());
}

void stage_move_message_callback(const stage_ros::moveActionGoal::ConstPtr& msg) {

}

void robot_position_callback(const nav_msgs::Odometry::ConstPtr& msg) {
	
}

int main (int argc, char** argv){
	//setup ros
	ros::init(argc, argv, "lta_test_goal_sender");
  	ros::NodeHandle n;

  	//subscribe to map metadata
  	ros::Subscriber sub = n.subscribe("/map_metadata", 100, mapDataCallback);

  	//setup movebase action client
   	actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> client("move_base", true);
   	while(!client.waitForServer(ros::Duration(5.0))){
    	ROS_INFO("Waiting for the move_base action server to come up");
   	}

   	while(!receivedMapData) {
   		//idle until data is received
   		ros::spinOnce();
   	}

   	
 	return 0;
 }