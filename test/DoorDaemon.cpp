//Door Daemon
#include <ros/ros.h>
#include <stdlib.h>
#include <cmath>
#include <fstream>
#include <iostream>
#ifndef STRING
#define STRING
#include <string>
#endif
#include <random>
#ifndef DOOR_H
#define DOOR_H
#include "door.h"
#endif
#include <chrono>
#include <thread>
 
#include <actionlib/client/simple_action_client.h>
#include <lta_sim_create_door/open_doorAction.h>

//#include <lta_semantic_map/UpdateDoorAction.h> //to send to sem map for testing


using namespace std;

const std::string POSITION_LABEL = "position:";
const std::string X_POSITION_LABEL = "x:";
const std::string Y_POSITION_LABEL = "y:";
const std::string THETA_POSITION_LABEL = "theta:";
const std::string WIDTH_LABEL = "width:";
const std::string STATUS_LABEL = "status:";
const std::string OPEN_PROB_LABEL = "open_prob:";
const std::string CLOSED_PROB_LABEL = "closed_prob:";

std::string door_db_filename = "~/catkin_ws/src/lta_semantic_map/test_doors.yaml";

std::map<int,Door*> door_db;
std::map<int,long> door_changes;

enum STAGE_DOOR_STATE { CLOSED = 0, OPEN = 1, TOGGLE = 2 };

bool load_door_db(const char* yaml){
	ROS_INFO("load door_db: %s", yaml);
	ifstream file(yaml);
	if (!file){
		return false;
	}
	string line;
	Door *door;
	bool first = true;
	while ( getline(file, line) ){ //read line by line
		size_t pos = 0;
	  	if ( (pos = line.find(POSITION_LABEL)) != string::npos ){
	 		//found position, read next three lines into x,y, theta
	 		getline(file, line);
	 		if ( (pos = line.find(X_POSITION_LABEL)) != string::npos ){
	 			double x = stod(line.substr(pos+X_POSITION_LABEL.length(),line.length()),NULL);
	 			getline(file, line);
	 			if ( (pos = line.find(Y_POSITION_LABEL)) != string::npos ){
	 				double y = stod(line.substr(pos+Y_POSITION_LABEL.length(),line.length()),NULL);
	 				getline(file, line);
	 				if ( (pos = line.find(THETA_POSITION_LABEL)) != string::npos ){
	 					double theta = stod(line.substr(pos+THETA_POSITION_LABEL.length(),line.length()),NULL);
	 					door->setPosition(Position(x,y,theta));
	 				}
	 			}
	 		}
	 	} else if ( (pos = line.find(WIDTH_LABEL)) != string::npos ){
	 		double width = stod(line.substr(pos+WIDTH_LABEL.length(),line.length()),NULL);
	 		door->setWidth(width);
	 	} else if ( (pos = line.find(STATUS_LABEL)) != string::npos ){
	 		if (line.find("OPEN",pos+STATUS_LABEL.length()) != string::npos){
	 			door->setOpen();
	 		} else if (line.find("CLOSED",pos+STATUS_LABEL.length()) != string::npos) {
	 			door->setClosed();
	 		} else{
	 			door->setUnknown();
	 		}
	 	} else if ( (pos = line.find(OPEN_PROB_LABEL)) != string::npos ){
	 		double prob = stod(line.substr(pos+OPEN_PROB_LABEL.length(),line.length()),NULL);
	 		door->setProbabiltyOpen(prob);
	 	} else if ( (pos = line.find(CLOSED_PROB_LABEL)) != string::npos ){
	 		double prob = stod(line.substr(pos+CLOSED_PROB_LABEL.length(),line.length()),NULL);
	 		door->setProbabiltyClosed(prob);
	 	} else { // is new door, id
	 		if ( (pos = line.find(":")) != string::npos ){
	 			int id = stoi(line.substr(0,pos),NULL);
	 			//cout<<id<<endl;
	 			if (first){
	 				first = false;
	 			} else {
	 				door_db[door->getID()] = door;
	 				door_changes[door->getID()] = 0;
	 			}
	 			door = new Door();
	 			door->setID(id);
	 		}
	 	}
	}
	door_db[door->getID()] = door;
	door_changes[door->getID()] = 0;
	file.close();
	ROS_INFO("Loaded %lu doors", door_db.size());
	return true;
}

int main (int argc, char** argv){
	if (argc<2){
		printf("Please call with yaml door database as argument!\n");
		return -1;
	}
	//setup ros
	ros::init(argc, argv, "lta_test_doordaemon");
  	ros::NodeHandle n;
  	actionlib::SimpleActionClient<lta_sim_create_door::open_doorAction> client("create_door", true); //set in stage object server (not working for me)
	// actionlib::SimpleActionClient<lta_semantic_map::UpdateDoorAction> client("update_door", true); //test setting in sem map
	
	client.waitForServer();

  	//load doors
	door_db_filename = argv[1];
	load_door_db(door_db_filename.c_str());
	
	//setup intervalls
	const int numSteps = 24;
	const std::chrono::seconds timeout(3);


	//setup door state change distribution/probabilities
	double baseProbability = 5.0;
	int probabilitiesBasedOnTimeSinceLastChange[numSteps];

	const int numSamplesForProbabilitysetup = 1000;
	int p[numSteps];
	for (int i=0; i<numSteps; i++) {
		p[i] = 0;
		probabilitiesBasedOnTimeSinceLastChange[i] = 0;
	}
	
	std::default_random_engine generator;
 	std::normal_distribution<double> distribution(6.0,4.0);

 	int numFittingSamples = 0;
 	while (numFittingSamples < numSamplesForProbabilitysetup) {
   		double number = distribution(generator);
   		if ((number>=0.0)&&(number<double(numSteps))) {
   			p[int(number)]+=1;
   			numFittingSamples++;
   		}
 	}

 	for (int i = 0; i<numSteps;i++) {
 		double prob = double(p[i]) / double(numSamplesForProbabilitysetup);
 		prob *= 100;
 		prob *= 5; //to get higher values, mutlipy with a 50% probability of a door state changing
 		probabilitiesBasedOnTimeSinceLastChange[i] = int(prob);
 		//std::cout<<i<<" : "<<probabilitiesBasedOnTimeSinceLastChange[i]<<std::endl;
 	}

	ROS_INFO("Door database contains %lu entries", door_db.size());
 	ROS_INFO("Door changes database contains %lu entries", door_changes.size());

 	//run daemon
	long iteration = 0;
	while(true) {
		for(auto &entry : door_changes) {
			std::cout<<"door #"<<entry.first<<std::endl;
			int value = rand() % 100;
			int timeSinceLastChange = iteration - entry.second;
			if (timeSinceLastChange > 23) {
				timeSinceLastChange = 23;
			}
			if (value < probabilitiesBasedOnTimeSinceLastChange[timeSinceLastChange]+baseProbability) {
				std::cout<<"  send toggle command"<<std::endl;
				//setup goal for stage object server (not working for me)
				lta_sim_create_door::open_doorGoal goal;
				goal.door_number = entry.first;
    			goal.state = TOGGLE;

				//setup goal for semantic map (for testing)
				// lta_semantic_map::UpdateDoorGoal goal;
				// goal.change_open = true;
				// if (door_db[entry.first]->isClosed()) {
				// 	goal.door_state=  "OPEN";
				// 	door_db[entry.first]->setOpen();
				// } else { 
				// 	goal.door_state= "CLOSED";
				// 	door_db[entry.first]->setClosed();
				// }
             	//-- 

             	client.sendGoal(goal);
	   			client.waitForResult();
	   			printf("Current State: %s\n", client.getState().toString().c_str());
				entry.second = iteration;
			}
		}
		iteration++;
    	std::this_thread::sleep_for(timeout);
	}

 	return 0;
}