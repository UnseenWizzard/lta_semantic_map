//Door Daemon
#include <ros/ros.h>
#include <stdlib.h>
#include <random>
#include <iostream>
#include <chrono>
#include <actionlib/client/simple_action_client.h>
#include <move_base_msgs/MoveBaseAction.h>
#include <nav_msgs/MapMetaData.h>

double minX = 0.0;
double maxX = 100.0;
double minY = 0.0;
double maxY = 100.0;
bool receivedMapData = false;

void mapDataCallback(const nav_msgs::MapMetaData::ConstPtr& msg) {
  maxX = msg->resolution * (double)msg->width;
  maxY = msg->resolution * (double)msg->height;
  receivedMapData = true;
}

int main (int argc, char** argv){
	if (argc<2){
		printf("Please specify number of goals to send robot to!\n");
		return -1;
	}
	int numRuns = atoi(argv[1]);
	int currentRun = 0;

	//setup ros
	ros::init(argc, argv, "lta_test_goal_sender");
  	ros::NodeHandle n;

  	//subscribe to map metadata
  	ros::Subscriber sub = n.subscribe("/map_metadata", 100, mapDataCallback);

  	//setup movebase action client
   	actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> client("move_base", true);
   	while(!client.waitForServer(ros::Duration(5.0))){
    	ROS_INFO("Waiting for the move_base action server to come up");
   	}

   	while(!receivedMapData) {
   		//idle until data is received
   		ros::spinOnce();
   	}

   	std::uniform_real_distribution<double> uniformX(minX, maxX);
   	std::uniform_real_distribution<double> uniformY(minY, maxY);
   	std::default_random_engine engine;

   	
	std::chrono::seconds::rep totalTime = 0;

   	//run
	while(currentRun < numRuns) {
		double randomX = uniformX(engine);
		double randomY = uniformY(engine);
		//random numbers limited to movebase map size & uniformly distributed

		move_base_msgs::MoveBaseGoal goal;

    	goal.target_pose.header.frame_id = "map";
    	goal.target_pose.header.stamp = ros::Time::now();

   		goal.target_pose.pose.position.x =  randomX;
   		goal.target_pose.pose.position.y =  randomY;
   		goal.target_pose.pose.position.z =  0.0;
   		goal.target_pose.pose.orientation.x = 0.0;
   		goal.target_pose.pose.orientation.y = 0.0;
   		goal.target_pose.pose.orientation.z = 0.0;
   		goal.target_pose.pose.orientation.w = 1.0;

   		ROS_INFO("Sending goal to reach (%f, %f)",randomX, randomY);
   		std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
   		client.sendGoal(goal);

	    client.waitForResult();

   		if(client.getState() == actionlib::SimpleClientGoalState::SUCCEEDED) {
   			std::chrono::steady_clock::time_point end= std::chrono::steady_clock::now();
      		ROS_INFO("Goal reached after %ld seconds", std::chrono::duration_cast<std::chrono::seconds>(end - begin).count());
      		currentRun++;
      		totalTime += std::chrono::duration_cast<std::chrono::seconds>(end - begin).count();
      		//return true;
   		}
   		else {
      		ROS_INFO("The robot could not reach the destination, sending another one.");
      		//return false;
   		}

	}
	ROS_INFO("Reached %d goals, in a total of %ld seconds", numRuns, totalTime);
 	return 0;
}