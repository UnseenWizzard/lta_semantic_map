# A Semantic Map Implementation for a Long-Term Autonomous Robot

**Door Semantic Map Layer for the LTA Project of the IST, TUGraz -- Bachelor's Project of Nicola Riedmann**

This is the source code to my Bachelor's Project/Thesis at TUGraz from 2016. 

It offers a semantic map extenstion to be used by a (canceled) long term autonomy robot project using ROS. 

It consist of a very basic semantic map holding information on doors in the environment of the robot. 
In addition to information about the existence (position) of doors, it offers a queriable probability for doors to be open or closed, which was intended to be integrated into smart planning of a patroul route through a building. 

For navigation purposes it offers a ROS costmap plugin, which places doors which are closed as lethal obstacles into the costmap.

## Modules

### lta_semantic_map_server

Main module, loads and publishes door database, offers action server to make changes to the db

(Will offer OPRS evaluable predicates and functions ?)

This MUST run for anything else to work properly! 

### lta_semantic_map_layer

Costmap2D layer plugin, containing all closed doors. Holds a copy of the door_db constructed from the servers DoorMap messages.

To use this, movebase must be launched from the launch file in this package. 
(`<include file="$(find lta_semantic_map)/launch/move_base.launch.xml">[args...]</include>`) 

### lta_semantic_map_cmdline_tool
Simple commandline tool to use the offered actions. (Add/Remove/Update door, store db)

### lta_semantic_map_oprs
Evaluable Predicates, Functions and Actions for OPRS. 
Include file and .opf defining the actions can be found in the oprs folder.
