#include <stdio.h>
#include <stdlib.h>
#include <ros/ros.h>
#include "std_msgs/String.h"

#include "macro-pub.h"
#include "opaque-pub.h"
#include "constant-pub.h"
#include "oprs-type-pub.h"
#include "oprs-type_f-pub.h"
#include "oprs_f-pub.h"

#include "pu-parse-tl_f.h"
#include "pu-mk-term_f.h"
#include "pu-genom_f.h"

#include "user-ev-function.h"
#include "ev-function_f-pub.h"
#include "user-ev-predicate.h"
#include "ev-predicate_f-pub.h"
#include "user-action.h"
#include "action_f-pub.h"

#include <actionlib/client/simple_action_client.h>
#include <lta_semantic_map/AddDoorAction.h>
#include <lta_semantic_map/UpdateDoorAction.h>
#include <lta_semantic_map/RemoveDoorAction.h>
#include <lta_semantic_map/SaveDoorDBAction.h> 
#include <lta_semantic_map/DoorEntry.h>
#include <lta_semantic_map/DoorMap.h>
#include "door.h"
#include <string>

//can't access oprs-type.h, but need the envar...
typedef enum {LOGICAL_VARIABLE, PROGRAM_VARIABLE} Variable_Type;
typedef struct type Type;
typedef Slist *SymList;
typedef Slist *TypeList;
struct type {
     Symbol name;
     Type *father;
     TypeList sur_types;  /* Plus itself */
     SymList elts;
};
struct envar {      /* Un envar */
     Symbol name;   /* son name */
     Term *value;   /* le term sur lequel elle pointe */
     Type *unif_type;   
     Variable_Type type BITFIELDS(:8);  /* Le type de la variable */
};
//--

typedef actionlib::SimpleActionClient<lta_semantic_map::AddDoorAction> AddClient;
typedef actionlib::SimpleActionClient<lta_semantic_map::RemoveDoorAction> RemoveClient;
typedef actionlib::SimpleActionClient<lta_semantic_map::UpdateDoorAction> UpdateClient;
typedef actionlib::SimpleActionClient<lta_semantic_map::SaveDoorDBAction> SaveClient;

std::map<int,Door*> door_db;
// AddClient a_client("add_door", true);
RemoveClient *r_client;//("remove_door", true);
UpdateClient *u_client;//("update_door", true);
ros::Subscriber sub;
// SaveClient  s_client("save_door_db", true);
ros::NodeHandle* n;
ros::AsyncSpinner* spinner;

void door_db_message_callback(const lta_semantic_map::DoorMap::ConstPtr& msg){
  std::cout<<"GOT DB MSG"<<std::endl;
  door_db.clear();
	for (auto &entry : msg->door_db){
        Door::Door_State state = Door::UNKNOWN;
        if (entry.door_state.find("OPEN")!=std::string::npos){
          state = Door::OPEN;
        } else if (entry.door_state.find("CLOSED")!=std::string::npos){
          state = Door::CLOSED;
        }
        door_db[entry.door_id] = new Door(entry.door_id, Position(entry.pos_x,entry.pos_y,entry.pos_theta), entry.width, state, entry.prob_open, entry.prob_closed);
    }
}

//predicates
  //door_state_known(x)
PBoolean door_state_known(TermList tl) {
	char* object;

    if (PUGetOprsParameters(tl, 1, ATOM, &object)) {
    	std::size_t delim_pos = std::string(object).find('_');
    	if (delim_pos != std::string::npos){ //found a _
    		if (std::string(object).compare(0,delim_pos,"DOOR")==0 ) { //is a door
    			int door_id = std::stoi(std::string(object).substr(delim_pos+1,std::string(object).length()));
    			//std::cout<<door_id<<std::endl;
          //std::cout<<"FOUND DOOR, state: "<<door_db[door_id]->getDoorState()<<std::endl;
          try{
            if (door_db[door_id]->getDoorState() != Door::UNKNOWN){
      				return TRUE;
      			}
          } catch (const std::out_of_range& oor){
            //failed, will return false below
          }
    		}
    	}
    }
    return FALSE;
}

  //door_open(x)
PBoolean door_open(TermList tl) {
	char* object;

    if (PUGetOprsParameters(tl, 1, ATOM, &object))
    {
    	std::size_t delim_pos = std::string(object).find('_');
    	if (delim_pos != std::string::npos){ //found a _
    		if (std::string(object).compare(0,delim_pos,"DOOR")==0){ //is a door
    			int door_id = std::stoi(std::string(object).substr(delim_pos+1,std::string(object).length()));
          try{
    			 if (door_db.at(door_id)->isOpen()){
    			   return TRUE;
    			 }
          } catch (const std::out_of_range& oor){
            //failed, will return false below
          }
    		}
    	}
    }
    return FALSE;
}
  //door_closed(x)
PBoolean door_closed(TermList tl) {
	char* object;

    if (PUGetOprsParameters(tl, 1, ATOM, &object))
    {
    	std::size_t delim_pos = std::string(object).find('_');
    	if (delim_pos != std::string::npos){ //found a _
    		if (std::string(object).compare(0,delim_pos,"DOOR")==0){ //is a door
    			int door_id = std::stoi(std::string(object).substr(delim_pos+1,std::string(object).length()));
          try {
            if (door_db.at(door_id)->isClosed()){
              return TRUE;
    			 }
          } catch (const std::out_of_range& oor){
            //failed, will return false below
            //std::cout<<"CAUGHT EXCEPT"<<std::endl;
          }
    		}
    	}
    }
    return FALSE;
}
//functions
  //door_open_probability(x)
  Term* door_open_probability(TermList tl){
    char* object;
    if (PUGetOprsParameters(tl, 1, ATOM, &object))
      {
        std::size_t delim_pos = std::string(object).find('_');
        if (delim_pos != std::string::npos){ //found a _
          if (std::string(object).compare(0,delim_pos,"DOOR")==0){ //is a door
            int door_id = std::stoi(std::string(object).substr(delim_pos+1,std::string(object).length()));
            try{
              return PUMakeTermFloat(door_db.at(door_id)->getProbabiltyOpen());
            } catch (const std::out_of_range& oor){
            //failed, will return false below
            } 
          }
        }
      }
    return PUMakeTermNil();
  }
  //door_closed_probability(x)
  Term* door_closed_probability(TermList tl){
    char* object;
    if (PUGetOprsParameters(tl, 1, ATOM, &object))
      {
        std::size_t delim_pos = std::string(object).find('_');
        if (delim_pos != std::string::npos){ //found a _
          if (std::string(object).compare(0,delim_pos,"DOOR")==0){ //is a door
            int door_id = std::stoi(std::string(object).substr(delim_pos+1,std::string(object).length()));
            try{
              return PUMakeTermFloat(door_db.at(door_id)->getProbabiltyClosed());
            } catch (const std::out_of_range& oor){
              //failed, will return false below
            }
          }
        }
      }
    return PUMakeTermNil();
  }
//get closed door
Term* get_closed_door(TermList tl) {
    Term* var = (Term*)get_list_pos(tl, 1);
    if ( var->type == VARIABLE )
    {
      for (auto &entry : door_db){
        if (entry.second->isClosed()){
          std::ostringstream stream;
          stream << "DOOR_";
          stream << entry.first;
          var->u.var->value = PUMakeTermAtom(const_cast<char*>(stream.str().c_str()));
          return PUMakeTermTrue();
        }
      }
    }
    return PUMakeTermNil();
}
//get open door
Term* get_open_door(TermList tl) {
    Term* var = (Term*)get_list_pos(tl, 1);
    if ( var->type == VARIABLE )
    {
      for (auto &entry : door_db){
        if (entry.second->isOpen()){
          std::ostringstream stream;
          stream << "DOOR_";
          stream << entry.first;
          var->u.var->value = PUMakeTermAtom(const_cast<char*>(stream.str().c_str()));
          return PUMakeTermTrue();
        }
      }
    }
    return PUMakeTermNil();
}

//actions
bool call_door_update(const int id, const bool set_as_open, const bool set_as_closed, const bool change_prob_open, const bool change_prob_closed, const double prob){
  lta_semantic_map::UpdateDoorGoal u_goal;
  u_goal.door_id = id;
  u_goal.change_pos = false;
  u_goal.change_width = false;
  if (set_as_open){
    u_goal.change_open = true;
    u_goal.door_state="OPEN";
  } else if (set_as_closed){
    u_goal.change_open = true;
    u_goal.door_state="CLOSED";
  } else{
    u_goal.change_open = false;
  }
  if (change_prob_open) {
    u_goal.change_prob_open = true;
    u_goal.prob_open = prob;
  } else {
    u_goal.change_prob_open = false;
  }
  if (change_prob_closed){
    u_goal.change_prob_closed = true;
    u_goal.prob_closed = prob;
  } else {
    u_goal.change_prob_closed = false;
  }
  u_client->sendGoal(u_goal);
  //std::cout<<"SENT GOAL"<<std::endl;
  //bool in_time = u_client->waitForResult(ros::Duration(10.0));
  //std::cout<<"GOT RESULT"<<std::endl;
  //std::cout<<u_client->getState().toString()<<std::endl;
  //std::cout<<"SUCCEEDED STATE?: "<<(u_client->getState() == actionlib::SimpleClientGoalState::SUCCEEDED)<<std::endl;
  //std::cout<<"RETURN SUCCESS: "<<u_client->getResult()->door_updated<<std::endl;
  return true;//(!in_time)?false:((*u_client->getResult()).door_updated);
}

  //set_door_closed(x)
Term* set_door_closed(TermList tl){
  char* object;
  if (PUGetOprsParameters(tl, 1, ATOM, &object))
    {
      std::size_t delim_pos = std::string(object).find('_');
      if (delim_pos != std::string::npos){ //found a _
        if (std::string(object).compare(0,delim_pos,"DOOR")==0){ //is a door
          int door_id = std::stoi(std::string(object).substr(delim_pos+1,std::string(object).length()));
          if (call_door_update(door_id,false,true,false,false,0.0)){
            //std::cout<<"OK"<<std::endl;
            return PUMakeTermTrue();
          }
        }
      }
    }
  return PUMakeTermNil();
}
  //set_door_open(x)
Term* set_door_open(TermList tl){
  char* object;
  if (PUGetOprsParameters(tl, 1, ATOM, &object))
    {
      std::size_t delim_pos = std::string(object).find('_');
      if (delim_pos != std::string::npos){ //found a _
        if (std::string(object).compare(0,delim_pos,"DOOR")==0){ //is a door
          int door_id = std::stoi(std::string(object).substr(delim_pos+1,std::string(object).length()));
          if (call_door_update(door_id,true,false,false,false,0.0)){
            //std::cout<<"OK"<<std::endl;
            return PUMakeTermTrue();
          }
        }
      }
    }
  //std::cout<<"FAILED"<<std::endl;
  return PUMakeTermNil();
}
  //set_open_probability(x)
Term* set_open_probability(TermList tl){
  char* object;
  double prob;
  if (PUGetOprsParameters(tl, 2, ATOM, &object, FLOAT, &prob))
    {
      std::size_t delim_pos = std::string(object).find('_');
      if (delim_pos != std::string::npos){ //found a _
        if (std::string(object).compare(0,delim_pos,"DOOR")==0){ //is a door
          int door_id = std::stoi(std::string(object).substr(delim_pos+1,std::string(object).length()));
            if (call_door_update(door_id,false,false,true,false,prob)){
              //std::cout<<"OK"<<std::endl;
              return PUMakeTermTrue();
            }
        }
      }
    }
  return PUMakeTermNil();
}
  //set_closed_probability(x)
Term* set_closed_probability(TermList tl){
  char* object;
  double prob;
  if (PUGetOprsParameters(tl, 2, ATOM, &object, FLOAT, &prob))
    {
      std::size_t delim_pos = std::string(object).find('_');
      if (delim_pos != std::string::npos){ //found a _
        if (std::string(object).compare(0,delim_pos,"DOOR")==0){ //is a door
          int door_id = std::stoi(std::string(object).substr(delim_pos+1,std::string(object).length()));
          
            if (call_door_update(door_id,false,false,false,true,prob)){
              //std::cout<<"OK"<<std::endl;
              return PUMakeTermTrue();
            }
          }
        }
      }
  return PUMakeTermNil();
}
  //remove_door(x)
Term* remove_door(TermList tl){
  char* object;
  if (PUGetOprsParameters(tl, 1, ATOM, &object))
    {
      std::size_t delim_pos = std::string(object).find('_');
      if (delim_pos != std::string::npos){ //found a _
        if (std::string(object).compare(0,delim_pos,"DOOR")==0){ //is a door
          int door_id = std::stoi(std::string(object).substr(delim_pos+1,std::string(object).length()));
          lta_semantic_map::RemoveDoorGoal r_goal;
          r_goal.door_id = door_id;
          r_client->sendGoal(r_goal);
          //bool in_time = r_client->waitForResult(ros::Duration(10.0));
          //if (!in_time && (*r_client->getResult()).door_removed){
            //std::cout<<"OK"<<std::endl;
            return PUMakeTermTrue();
          //}
        }
      }
    }
  return PUMakeTermNil();
}
  //OTHER UPDATE FUNCTIONS NEEDED?

void declare_user_eval_funct(void) { 
	make_and_declare_eval_funct("PROBABILITY_DOOR_OPEN", door_open_probability, 1); 
  make_and_declare_eval_funct("PROBABILITY_DOOR_CLOSED", door_closed_probability, 1); 
  make_and_declare_eval_funct("GET_CLOSED_DOOR", get_closed_door, 1); 
  make_and_declare_eval_funct("GET_OPEN_DOOR", get_open_door, 1); 
	/*void make and declare eval funct (Function name, PFPT funct, int ar) is used to declare the evaluable function. 
	You have to specify the function name, the C function which implements it, and the arity of this function. */
	return; 
}

void declare_user_eval_pred(void){
	make_and_declare_eval_pred("DOOR_STATE_KNOWN", door_state_known, 1, TRUE);
  make_and_declare_eval_pred("DOOR_OPEN", door_open, 1, FALSE);
  make_and_declare_eval_pred("DOOR_CLOSED", door_closed, 1, FALSE);
	/*void make and declare eval pred (Predicat name, PFB pred, int ar, PBoolean cwp) is used to declare the evaluable predicate. 
	You have to specify the predicate name, the C function which implements it, the arity of this predicate and a boolean to indicate 
	if the predicate is a closed world predicate. */
	return;
}

void declare_user_action(void){
	make_and_declare_action("REMOVE_DOOR", remove_door, 1); 
  make_and_declare_action("SET_DOOR_OPEN", set_door_open, 1);
  make_and_declare_action("SET_DOOR_CLOSED", set_door_closed, 1);
  make_and_declare_action("SET_OPEN_PROBABILITY", set_open_probability, 2);
  make_and_declare_action("SET_CLOSED_PROBABILITY", set_closed_probability, 2);
	/*void make and declare action (Function name, PFPT funct, int ar) is used to declare an Action. 
	You have to specify the function name, the C function which implements it, and the arity of this function.*/
	return;
}

//init function
/*
	starts the door map listener (ros node)
	declares all user definded oprs functions
*/
extern "C" void init_semantic_map_oprs(void)
{
  int argc = 0;
	 ros::init(argc, (char**)NULL, "lta_semantic_map_oprs_interface");
   n = new ros::NodeHandle();
   spinner = new ros::AsyncSpinner(1);
   sub = n->subscribe("/lta_semantic_map_door_db",500, door_db_message_callback);
   r_client = new RemoveClient("remove_door", true);
   u_client = new UpdateClient("update_door", true);
   spinner->start(); 
   u_client->waitForServer();
   r_client->waitForServer();
	 declare_user_action();
   declare_user_eval_funct();
   declare_user_eval_pred();
}