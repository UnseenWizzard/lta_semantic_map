#include "position.h"
#include <ros/ros.h>
#include <actionlib/client/simple_action_client.h>
#include <lta_semantic_map/AddDoorAction.h>
#include <lta_semantic_map/UpdateDoorAction.h>
#include <lta_semantic_map/RemoveDoorAction.h>
#include <lta_semantic_map/SaveDoorDBAction.h>

using namespace std;

typedef actionlib::SimpleActionClient<lta_semantic_map::AddDoorAction> AddClient;
typedef actionlib::SimpleActionClient<lta_semantic_map::RemoveDoorAction> RemoveClient;
typedef actionlib::SimpleActionClient<lta_semantic_map::UpdateDoorAction> UpdateClient;
typedef actionlib::SimpleActionClient<lta_semantic_map::SaveDoorDBAction> SaveClient;

void add_door(AddClient *a_client){
  lta_semantic_map::AddDoorGoal a_goal;
  cout << "Please enter door params:";
  cout << "\nPosition X: ";
  cin >> a_goal.pos_x;
  cout << "\nPosition Y: ";
  cin >>a_goal.pos_y;
  cout << "\nPosition theta: ";
  cin >> a_goal.pos_theta;
  cout << "\nWidth: ";
  cin >> a_goal.width;
  cout << "\nOpen (y/n/u(nknown)): ";
  string reply = "u";
  cin >> reply;
  if (reply == "y"){
    a_goal.door_state="OPEN";
  } else if (reply == "n"){
    a_goal.door_state="CLOSED";
  } else {
    a_goal.door_state="UNKNOWN";
  }
  cout << "\nProbabilty Open: ";
  cin >> a_goal.prob_open;
  cout << "\nProbabilty Closed: ";
  cin >> a_goal.prob_closed;
  a_client->sendGoal(a_goal);
  a_client->waitForResult(ros::Duration(5.0));
  if (a_client->getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
    printf("Yay! The door was added\t");
  printf("Current State: %s\n", a_client->getState().toString().c_str());
}
void remove_door(RemoveClient *r_client){
  lta_semantic_map::RemoveDoorGoal r_goal;
  cout << "Please enter id of door to be removed: ";
  cin >> r_goal.door_id;
  r_client->sendGoal(r_goal);
  r_client->waitForResult(ros::Duration(5.0));
  if (r_client->getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
    printf("Yay! The door was removed\t");
  printf("Current State: %s\n", r_client->getState().toString().c_str());
}
void update_door(UpdateClient *u_client){
  lta_semantic_map::UpdateDoorGoal u_goal;
  cout << "Please enter id of door to be updated: ";
  cin >> u_goal.door_id;
  string reply;
  cout << "\nChange Position? (y/n): ";
  cin >> reply;
  if (reply == "y"){
    u_goal.change_pos = true;
    cout << "\nPosition X: ";
    cin >> u_goal.pos_x;
    cout << "\nPosition Y: ";
    cin >>u_goal.pos_y;
    cout << "\nPosition theta: ";
    cin >> u_goal.pos_theta;
  } else {
    u_goal.change_pos = false;
  }
  cout << "\nChange Width? (y/n): ";
  cin >> reply;
  if (reply == "y"){
    u_goal.change_width = true;
    cout << "\nWidth: ";
    cin >> u_goal.width;
  } else {
    u_goal.change_width = false;
  } 
  cout << "\nChange Open State? (y/n): ";
  cin >> reply;
  if (reply == "y"){
    u_goal.change_open = true;
    cout << "\nOpen (y/n/u(nknown)): ";
    cin >> reply;
    if (reply == "y"){
      u_goal.door_state="OPEN";
    } else if (reply == "n"){
      u_goal.door_state="CLOSED";
    } else {
      u_goal.door_state="UNKNOWN";
    }
  } else {
    u_goal.change_open = false;
  }   
  cout << "\nChange Probabilty Open? (y/n): ";
  cin >> reply;
  if (reply == "y"){
    u_goal.change_prob_open = true;
    cout << "\nProbabilty Open: ";
    cin >> u_goal.prob_open;
  } else {
    u_goal.change_prob_open = false;
  }
  cout << "\nChange Probabilty Closed? (y/n): ";
  cin >> reply;
  if (reply == "y"){
    u_goal.change_prob_closed = true;
    cout << "\nProbabilty Open: ";
    cin >> u_goal.prob_closed;
  } else {
    u_goal.change_prob_closed = false;
  }  
  u_client->sendGoal(u_goal);
  u_client->waitForResult(ros::Duration(5.0));
  if (u_client->getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
    printf("Yay! The door was updated\t");
  printf("Current State: %s\n", u_client->getState().toString().c_str());
}
void save_db(SaveClient *s_client){
  lta_semantic_map::SaveDoorDBGoal s_goal;
  cout << "Please enter a file path to save to: ";
  cin >> s_goal.file_name;
  s_client->sendGoal(s_goal);
  s_client->waitForResult(ros::Duration(5.0));
  if (s_client->getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
    printf("Yay! The doors were saved");
  printf("Current State: %s\n", s_client->getState().toString().c_str());
}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "door_action_test_client");
  
  AddClient a_client("add_door", true); // true -> don't need ros::spin()
  a_client.waitForServer();
  
  UpdateClient u_client("update_door", true); // true -> don't need ros::spin()
  u_client.waitForServer();
  
  RemoveClient r_client("remove_door", true); // true -> don't need ros::spin()
  r_client.waitForServer();
  
  SaveClient s_client("save_door_db", true); // true -> don't need ros::spin()
  s_client.waitForServer();
  
  bool running = true;
  cout << "Semantic Map Door DB Action Client Tool running." << endl; 
  while (running){
    cout << "Please choose an action: \n 1. Add Door \n 2. Update Door \n 3. Remove Door \n 4. Save Database \n 5. exit" << endl;
    int choice;
    cin >> choice;
    switch(choice){
      case 1: add_door(&a_client); break;
      case 2: update_door(&u_client); break;
      case 3: remove_door(&r_client); break;
      case 4: save_db(&s_client); break;
      case 5: running=false; break;
      default: 
        cin.clear(); 
        cin.ignore(1000,'\n');
        break;
    }
  }
  return 0;
}