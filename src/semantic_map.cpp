#include "semantic_map.h"
using namespace std;

bool load_door_db(const char* yaml){
	ROS_INFO("load door_db: %s", yaml);
	ifstream file(yaml);
	if (!file){
		return false;
	}
	string line;
	Door *door;
	bool first = true;
	while ( getline(file, line) ){ //read line by line
		size_t pos = 0;
	  	if ( (pos = line.find(POSITION_LABEL)) != string::npos ){
	 		//found position, read next three lines into x,y, theta
	 		getline(file, line);
	 		if ( (pos = line.find(X_POSITION_LABEL)) != string::npos ){
	 			double x = stod(line.substr(pos+X_POSITION_LABEL.length(),line.length()),NULL);
	 			getline(file, line);
	 			if ( (pos = line.find(Y_POSITION_LABEL)) != string::npos ){
	 				double y = stod(line.substr(pos+Y_POSITION_LABEL.length(),line.length()),NULL);
	 				getline(file, line);
	 				if ( (pos = line.find(THETA_POSITION_LABEL)) != string::npos ){
	 					double theta = stod(line.substr(pos+THETA_POSITION_LABEL.length(),line.length()),NULL);
	 					door->setPosition(Position(x,y,theta));
	 				}
	 			}
	 		}
	 	} else if ( (pos = line.find(WIDTH_LABEL)) != string::npos ){
	 		double width = stod(line.substr(pos+WIDTH_LABEL.length(),line.length()),NULL);
	 		door->setWidth(width);
	 	} else if ( (pos = line.find(STATUS_LABEL)) != string::npos ){
	 		if (line.find("OPEN",pos+STATUS_LABEL.length()) != string::npos){
	 			door->setOpen();
	 		} else if (line.find("CLOSED",pos+STATUS_LABEL.length()) != string::npos) {
	 			door->setClosed();
	 		} else{
	 			door->setUnknown();
	 		}
	 	} else if ( (pos = line.find(OPEN_PROB_LABEL)) != string::npos ){
	 		double prob = stod(line.substr(pos+OPEN_PROB_LABEL.length(),line.length()),NULL);
	 		door->setProbabiltyOpen(prob);
	 	} else if ( (pos = line.find(CLOSED_PROB_LABEL)) != string::npos ){
	 		double prob = stod(line.substr(pos+CLOSED_PROB_LABEL.length(),line.length()),NULL);
	 		door->setProbabiltyClosed(prob);
	 	} else { // is new door, id
	 		if ( (pos = line.find(":")) != string::npos ){
	 			int id = stoi(line.substr(0,pos),NULL);
	 			//cout<<id<<endl;
	 			if (first){
	 				first = false;
	 			} else {
	 				door_db[door->getID()] = door;
	 			}
	 			door = new Door();
	 			door->setID(id);
	 			// cout<<"curr id: "<<id<<endl;
	 			// cout<<"db size: "<<door_db.size()<<endl;
	 			// if (id+1 > door_db.size()){
	 			// 	door_db.resize(id);
	 			// }
	 			// if (door->getID() < 0){ //first door
	 			// 	door->setID(id);
	 			// } else {
	 			// 	//store door
	 			// 	door_db.insert(door_db.begin()+door->getID(),door);
	 			// 	//make new door
	 			// 	door = new Door();
	 			// 	door->setID(id);
	 			// }
	 		}
	 	}
	}
	door_db[door->getID()] = door;
	file.close();
	ROS_INFO("Loaded %lu doors", door_db.size());
	db_publisher.publish(make_db_message());
	return true;
}

bool store_door_db(string yaml){
	ofstream file(yaml);
	if (!file){
		ROS_INFO("store failed");
		return false;
	}
	//for(map<int,Door*>::iterator it = door_db.begin(); it != door_db.end(); it++) {
	for (auto &entry : door_db){
		file << entry.second->getYamlString();
	}
	file.close();
	return true;
}

int add_door(const Position position, const double width, const Door::Door_State state, const double prob_open, const double prob_closed){
	int next_id = door_db.rbegin()->first+1;
	cout<<next_id<<endl;
	door_db[next_id] = new Door(next_id, position, width, state, prob_open, prob_closed);
	if (next_id == door_db.size()){
		return -1;
	} else {
		return next_id;
	}
}

bool remove_door(int id){
	Door* d = door_db[id];
	if (d == NULL) { return false; }
	delete d;
	door_db.erase(id);
	return true;
}

bool update_door_pos(int id, const Position position){
	Door* d = door_db[id];
	if (d == NULL) { return false; }
	d->setPosition(position);
	return true;
}
bool update_door_width(int id, const double width){
	Door* d = door_db[id];
	if (d == NULL) { return false; }
	d->setWidth(width);
	return true;
}
bool update_door_open(int id, const Door::Door_State state){
	Door* d = door_db[id];
	if (d == NULL) { return false; }
	d->setDoorState(state);
	return true;
}
bool update_door_prob_open(int id, const double prob_open){
	Door* d = door_db[id];
	if (d == NULL) { return false; }
	d->setProbabiltyOpen(prob_open);
	return true;
}
bool update_door_prob_closed(int id, const double prob_closed){
	Door* d = door_db[id];
	if (d == NULL) { return false; }
	d->setProbabiltyClosed(prob_closed);
	return true;
}

void execute_add(const lta_semantic_map::AddDoorGoalConstPtr& goal, AddServer* as){
	ROS_INFO("Executing AddDoor action");
	Door::Door_State state = Door::UNKNOWN;
	if (goal->door_state.find("OPEN")!=string::npos){
		state = Door::OPEN;
	} else if (goal->door_state.find("CLOSED")!=string::npos){
		state = Door::CLOSED;
	}
	int door_id = add_door(Position(goal->pos_x,goal->pos_y,goal->pos_theta),goal->width,state,goal->prob_open,goal->prob_closed);
	if (door_id < 0){
		ROS_INFO("AddDoor action failed");
		as->setAborted();
	} else {
		lta_semantic_map::AddDoorResult result;
		result.door_id = door_id;
		ROS_INFO("AddDoor action successfull");
		//store_door_db(door_db_filename);
		db_publisher.publish(make_db_message());
		as->setSucceeded(result);
	}
}

void execute_remove(const lta_semantic_map::RemoveDoorGoalConstPtr& goal, RemoveServer* as){
	ROS_INFO("Executing RemoveDoor action");
	bool success = remove_door(goal->door_id);
	if (!success){
		ROS_INFO("RemoveDoor action failed");
		as->setAborted();
	} else {
		lta_semantic_map::RemoveDoorResult result;
		result.door_removed = success;
		ROS_INFO("RemoveDoor action successfull");
		//store_door_db(door_db_filename);
		db_publisher.publish(make_db_message());
		as->setSucceeded(result);
	}
}
void execute_update(const lta_semantic_map::UpdateDoorGoalConstPtr& goal, UpdateServer* as){
	ROS_INFO("Executing UpdateDoor action");
	bool success = true;
	if ( goal->change_pos ){
		success = update_door_pos(goal->door_id,Position(goal->pos_x,goal->pos_y,goal->pos_theta));
	}
	if ( goal->change_width ){
		success = update_door_width(goal->door_id,goal->width);
	}
	if ( goal->change_open ){
		Door::Door_State state = Door::UNKNOWN;
		if (goal->door_state.find("OPEN")!=std::string::npos){
			state = Door::OPEN;
		} else if (goal->door_state.find("CLOSED")!=std::string::npos){
			state = Door::CLOSED;
		}
		success = update_door_open(goal->door_id,state);
	}
	if ( goal->change_prob_open ){
		success = update_door_prob_open(goal->door_id,goal->prob_open);
	}
	if ( goal->change_prob_closed ){
		success = update_door_prob_closed(goal->door_id,goal->prob_closed);
	}

	if (!success){
		ROS_INFO("UpdateDoor action failed");
		as->setAborted();
	} else {
		lta_semantic_map::UpdateDoorResult result;
		result.door_updated = success;
		ROS_INFO("UpdateDoor action successfull");
		//store_door_db(door_db_filename);
		db_publisher.publish(make_db_message());
		as->setSucceeded(result);
	}
}

void execute_storedb(const lta_semantic_map::SaveDoorDBGoalConstPtr& goal, SaveDBServer* as){
	ROS_INFO("Executing Store DB action");
	bool success = store_door_db(goal->file_name);
	if (!success){
		ROS_INFO("StoreDB action failed");
		as->setAborted();
	} else {
		lta_semantic_map::SaveDoorDBResult result;
		result.stored_successful = success;
		ROS_INFO("Save door db successfull");
		//store_door_db("internal_doors.yaml");
		db_publisher.publish(make_db_message());
		as->setSucceeded(result);
	}
}

lta_semantic_map::DoorMap make_db_message(){
	lta_semantic_map::DoorMap db_msg;// = new lta_semantic_map::DoorMap();
	db_msg.door_db.clear();
	for (auto &door : door_db){
		lta_semantic_map::DoorEntry entry;
		entry.door_id = door.second->getID();
		entry.pos_x = door.second->getPosition().getX();
		entry.pos_y = door.second->getPosition().getY();
		entry.pos_theta = door.second->getPosition().getTheta();
		entry.width = door.second->getWidth();
		// entry.open = door.second->isOpen();
		switch(door.second->getDoorState()){
			case Door::OPEN: entry.door_state="OPEN"; break;
			case Door::CLOSED: entry.door_state="CLOSED"; break;
			case Door::UNKNOWN: entry.door_state="UNKNOWN"; break;
		}
		entry.prob_open = door.second->getProbabiltyOpen();
		entry.prob_closed = door.second->getProbabiltyClosed();
		db_msg.door_db.push_back(entry);
	}
	return db_msg;
}

//OPRS evaluable functions
//door open ?
//door open prob ?
//door closed prob ?

int main (int argc, char** argv){
	if (argc<2){
		printf("Please call with yaml door database as argument!\n");
		return -1;
	}
	ros::init(argc, argv, "lta_semantic_map");
  	ros::NodeHandle n;
  	db_publisher = n.advertise<lta_semantic_map::DoorMap>("lta_semantic_map_door_db",1000,true);
	//printf("Will load db from: %s\n", argv[1]);
	door_db_filename = argv[1];
	load_door_db(door_db_filename.c_str());
	store_door_db("internal_doors.yaml");
  	AddServer add_server(n, "add_door", boost::bind(&execute_add, _1, &add_server), false);
  	RemoveServer remove_server(n, "remove_door", boost::bind(&execute_remove, _1, &remove_server), false);
  	UpdateServer update_server(n, "update_door", boost::bind(&execute_update, _1, &update_server), false);
  	SaveDBServer save_server(n, "save_door_db", boost::bind(&execute_storedb, _1, &save_server), false);
  	add_server.start();
  	remove_server.start();
  	update_server.start();
  	save_server.start();
  	ros::spin();
  	
  	return 0;
}