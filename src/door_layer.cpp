#include "door_layer.h"
#include <pluginlib/class_list_macros.h>
using costmap_2d::LETHAL_OBSTACLE;
using costmap_2d::NO_INFORMATION;
using costmap_2d::FREE_SPACE;

PLUGINLIB_EXPORT_CLASS(lta_semantic_layer_namespace::DoorLayer, costmap_2d::Layer);


namespace lta_semantic_layer_namespace{ //todo rename to door_layer_namespace
	DoorLayer::DoorLayer(){};

	Position DoorLayer::get_end_point(Position p, double width){
		double to_rad = 3.14159265/180.0;
		return Position(p.getX()+width*cos(p.getTheta()*to_rad),p.getY()+width*sin(p.getTheta()*to_rad),0);
	}

	void DoorLayer::door_db_message_callback(const lta_semantic_map::DoorMap::ConstPtr& msg){
		ROS_INFO("GOT DB MSG");
    reset();
		for (auto &entry : msg->door_db){
        Door::Door_State state = Door::UNKNOWN;
        if (entry.door_state.find("OPEN")!=std::string::npos){
          state = Door::OPEN;
        } else if (entry.door_state.find("CLOSED")!=std::string::npos){
          state = Door::CLOSED;
        }
		  	door_db[entry.door_id] = new Door(entry.door_id, Position(entry.pos_x,entry.pos_y,entry.pos_theta), entry.width, state, entry.prob_open, entry.prob_closed);
        //ROS_INFO("Storing door %d",entry.door_id);
		}
    ROS_INFO("DB size %lu",door_db.size());
	}

  void DoorLayer::reset() {
    ROS_INFO("reset called");
    deactivate();
    resetMaps();
    door_db.clear();
    current_ = true;
    activate();
  }

	void DoorLayer::onInitialize(){
		current_ = true;
		default_value_ = NO_INFORMATION;
		matchSize();
		sub = nh.subscribe("/lta_semantic_map_door_db",1000, &DoorLayer::door_db_message_callback, this);
		dsrv_ = new dynamic_reconfigure::Server<costmap_2d::GenericPluginConfig>(nh);
		dynamic_reconfigure::Server<costmap_2d::GenericPluginConfig>::CallbackType callback = boost::bind(&DoorLayer::reconfigureCB, this, _1, _2);
		dsrv_->setCallback(callback);
    //ros::AsyncSpinner spinner(1);
    //spinner.start();
		//ros::spinOnce();
	}

	void DoorLayer::matchSize(){
		Costmap2D* master = layered_costmap_->getCostmap();
		resizeMap(master->getSizeInCellsX(), master->getSizeInCellsY(), master->getResolution(), master->getOriginX(), master->getOriginY());
	}

	void DoorLayer::reconfigureCB(costmap_2d::GenericPluginConfig &config, uint32_t level){	
  		enabled_ = config.enabled;
	}

	void DoorLayer::updateBounds(double robot_x, double robot_y, double robot_yaw, double* min_x, double* min_y, double* max_x, double* max_y){
      int map_width = layered_costmap_->getCostmap()->getSizeInCellsX();
      int map_height = layered_costmap_->getCostmap()->getSizeInCellsY();
  		if (!enabled_) { return; }
   		for (auto &entry : door_db){
       
  			//bresenham compact
  			Position start = entry.second->getPosition();
  			Position end = get_end_point(start, entry.second->getWidth());
  			uint ux0, uy0, ux1, uy1;
  			worldToMap(start.getX(),start.getY(),ux0,uy0);
  			worldToMap(end.getX(),end.getY(),ux1,uy1);
        int x0 = (int)ux0;
        int y0 = (int)uy0;
        int x1 = (int)ux1;
        int y1 = (int)uy1;

        //Proper idea, but not working as intended, and adding/substracting is an ugly&unreliable fix
        // *min_x = std::min({*min_x, (double)ux0, (double)ux1})-100;
        // *min_y = std::min({*min_y, (double)uy0, (double)uy1})-100;
        // *max_x = std::max({*max_x, (double)ux0, (double)ux1})+70;
        // *max_y = std::max({*max_y, (double)uy0, (double)uy1})+70;

        // *min_x = ( *min_x < 0 ) ? 0 : *min_x;
        // *min_y = ( *min_y < 0 ) ? 0 : *min_y;
        // *max_x = ( *max_x > layered_costmap_->getCostmap()->getSizeInCellsX() ) ? layered_costmap_->getCostmap()->getSizeInCellsX() : *max_x;
        // *max_y = ( *max_y > layered_costmap_->getCostmap()->getSizeInCellsY() ) ? layered_costmap_->getCostmap()->getSizeInCellsY() : *max_y;      

  			int dx = abs(x1-x0);
  			int dy = -abs(y1-y0);
  			int sx = x0<x1?1:-1;
  			int sy = y0<y1?1:-1;
  			int err = dx+dy;
  			int e2;
  			while(1){
  				if (x0 <= map_width && y0 <= map_height){
          //ROS_INFO("(%d,%d)",x0,y0);
            if (entry.second->isClosed()){
  				    setCost(x0, y0, LETHAL_OBSTACLE);
              //ROS_INFO("Door %d drawn",entry.second->getID());
            } else if (entry.second->isOpen()){
              setCost(x0, y0, FREE_SPACE);
              //ROS_INFO("Door %d freed",entry.second->getID());
            } else {
              setCost(x0, y0, NO_INFORMATION);
            }
  				}
  				if (x0==x1 && y0==y1){
  					break;
  				}
  				e2 = 2*err;
  				if (e2 > dy){
  					err += dy;
  					x0 += sx;
  				}
  				if (e2 < dx){
  					err += dx;
  					y0 += sy;
  				}
  			}
		}
    //As setting the minimum coordinates reached by doors does not work, simply force entry of complete costmap grid! (slightly slow)
    *min_x = 0;
    *min_y = 0;
    *max_x = layered_costmap_->getCostmap()->getSizeInCellsX();
    *max_y = layered_costmap_->getCostmap()->getSizeInCellsY();
	}

	void DoorLayer::updateCosts(costmap_2d::Costmap2D& master_grid, int min_i, int min_j, int max_i, int max_j){
    //ROS_INFO("UPDATE COSTS");
  		if (!enabled_){ return; }
  		for (int j = min_j; j < max_j; j++){
    		for (int i = min_i; i < max_i; i++){
      			int index = getIndex(i, j);
      			if (costmap_[index] == NO_INFORMATION)
        		  continue;
      			master_grid.setCost(i, j, costmap_[index]); 
    		}
  		}
	}

}