#include "door.h"

using namespace std;

Door::Door(){
	this->id = -1;
	this->position = Position();
	this->width = 0;
	this->state = Door::UNKNOWN;
	this->prob_open = 0;
	this->prob_closed = 0;
}

Door::Door(const int id, const Position position, const double width, const Door::Door_State state, const double prob_open, const double prob_closed){
	this->id = id;
	this->position = position;
	this->width = width;
	this->state = state;
	this->prob_open = prob_open;
	this->prob_closed = prob_closed;
}

int Door::getID() const{
	return this->id;
}

Position Door::getPosition() const{
	return this->position;
}

double Door::getWidth() const{
	return this->width;
}

bool Door::isOpen() const{
	if (this->state == Door::OPEN){
		return true;
	}
	return false;
}

bool Door::isClosed() const{
	if (this->state == Door::CLOSED){
		return true;
	}
	return false;
}

Door::Door_State Door::getDoorState() const {
	return this->state;
}

double Door::getProbabiltyOpen() const{
	return this->prob_open;
}

double Door::getProbabiltyClosed() const{
	return this->prob_closed;
}

string Door::getYamlString() const{
	ostringstream stream;
	stream << this->id << ":\n    position: \n        x: " << this->position.getX() << "\n        y: " << this->position.getY() << "\n        theta: " << this->position.getTheta() << "\n    width: " << this->width;
	//if (this->open) {
	//	stream << "\n    status: OPEN"; 	
	//} else {
	//	stream << "\n    status: CLOSED"; 	
	//}
	switch(state){
		case Door::OPEN: stream << "\n    status: OPEN"; break;
		case Door::CLOSED: stream << "\n    status: CLOSED"; break;
		case Door::UNKNOWN: stream << "\n    status: UNKNOWN"; break;
	}
	stream << "\n    closed_prob: " << this->prob_closed << "\n    open_prob: " << this->prob_open << endl; 
	return stream.str();
}

void Door::setID(const int id){
	this->id =  id;
}

void Door::setPosition(const Position pos){
	this->position = pos;
}

void Door::setWidth(const double width){
	this->width = width;
}

void Door::setOpen(){
	this->state = Door::OPEN;
}

void Door::setClosed(){
	this->state = Door::CLOSED;
}

void Door::setUnknown(){
	this->state = Door::UNKNOWN;
}

void Door::setDoorState(const Door::Door_State state){
	this->state = state;
}

void Door::setProbabiltyOpen(const double probabilty){
	this->prob_open = probabilty;
}

void Door::setProbabiltyClosed(const double probabilty){
	this->prob_closed = probabilty;
}

// int main(int argc, char** argv){
// 	Door d = Door(7,Position(45.3,43.2,89),100,true,0.5,0.5);
//  	cout << d.getYamlString();
// }