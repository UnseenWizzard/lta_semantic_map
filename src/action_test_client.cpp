#include "position.h"
#include <ros/ros.h>
#include <actionlib/client/simple_action_client.h>
#include <lta_semantic_map/AddDoorAction.h>
#include <lta_semantic_map/UpdateDoorAction.h>
#include <lta_semantic_map/RemoveDoorAction.h>
#include <lta_semantic_map/SaveDoorDBAction.h>

typedef actionlib::SimpleActionClient<lta_semantic_map::AddDoorAction> AddClient;
typedef actionlib::SimpleActionClient<lta_semantic_map::RemoveDoorAction> RemoveClient;
typedef actionlib::SimpleActionClient<lta_semantic_map::UpdateDoorAction> UpdateClient;
typedef actionlib::SimpleActionClient<lta_semantic_map::SaveDoorDBAction> SaveClient;

int main(int argc, char** argv)
{
  ros::init(argc, argv, "door_action_test_client");
  //test add
  AddClient a_client("add_door", true); // true -> don't need ros::spin()
  a_client.waitForServer();
  lta_semantic_map::AddDoorGoal a_goal;
  // Fill in goal here
  a_goal.pos_x = 45.0;
  a_goal.pos_y =45.0;
  a_goal.pos_theta=90.0;
  a_goal.width = 70.0;
  a_goal.door_state="OPEN";
  a_goal.prob_open=0.5;
  a_goal.prob_closed=0.5;
  a_client.sendGoal(a_goal);
  a_client.waitForResult(ros::Duration(5.0));
  if (a_client.getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
    printf("Yay! The door was added\t");
  printf("Current State: %s\n", a_client.getState().toString().c_str());
  int id = a_client.getResult()->door_id;
  
  // //test update
  UpdateClient u_client("update_door", true); // true -> don't need ros::spin()
  u_client.waitForServer();
  lta_semantic_map::UpdateDoorGoal u_goal;
  // Fill in goal here
  u_goal.change_pos = false;
  u_goal.change_width = true;
  u_goal.change_open = false;
  u_goal.change_prob_open = true;
  u_goal.change_prob_closed = false;
  u_goal.door_id = id;
  //u_goal.pos_x = 0.0;
  //u_goal.pos_y = 0.0;
  //u_goal.pos_theta= 0.0;
  u_goal.width = 180.0;
  //u_goal.door_state="FALSE";
  u_goal.prob_open=1.0;
  //u_goal.prob_closed=0.5;
  u_client.sendGoal(u_goal);
  u_client.waitForResult(ros::Duration(5.0));
  if (u_client.getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
    printf("Yay! The door was updated\t");
  printf("Current State: %s\n", u_client.getState().toString().c_str());

  // //test remove
  RemoveClient r_client("remove_door", true); // true -> don't need ros::spin()
  r_client.waitForServer();
  lta_semantic_map::RemoveDoorGoal r_goal;
  // Fill in goal here
  r_goal.door_id = id;
  r_client.sendGoal(r_goal);
  r_client.waitForResult(ros::Duration(5.0));
  if (r_client.getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
    printf("Yay! The door was removed\t");
  printf("Current State: %s\n", r_client.getState().toString().c_str());
  
  //test save
  SaveClient s_client("save_door_db", true); // true -> don't need ros::spin()
  s_client.waitForServer();
  lta_semantic_map::SaveDoorDBGoal s_goal;
    // Fill in goal here
  s_goal.file_name = "test.yaml";
  s_client.sendGoal(s_goal);
  s_client.waitForResult(ros::Duration(5.0));
  if (s_client.getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
    printf("Yay! The doors were saved");
  printf("Current State: %s\n", s_client.getState().toString().c_str());

  return 0;
}