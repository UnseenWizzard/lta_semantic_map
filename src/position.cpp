#include "position.h"

using namespace std;

Position::Position(){
	x = 0;
	y = 0;
	theta = 0;
};

Position::Position(const double x,const double y,const double theta){
	this->x = x;
	this->y = y;
	this->theta = theta;
}
	
/*Position::Position(geometry_msgs::Pose2D &pose){
	pose->x = this.x;
	pose->y = this.y;
	pose->theta = this.theta;
}*/


double Position::getX() const{
	return this->x;
}

double Position::getY() const{
	return this->y;
}

double Position::getTheta() const{
	return this->theta;
}

void Position::setX(const double x){
	this->x = x;
}

void Position::setY(const double y){
	this->y = y;
}

void Position::setTheta(const double theta){
	this->theta = theta;
}